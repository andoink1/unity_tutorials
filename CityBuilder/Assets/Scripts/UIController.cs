﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    private City city;

    [SerializeField]
    private Text cityText;
    [SerializeField]
    private Text dayText;

    // Start is called before the first frame update
    void Start()
    {
        //city = FindObjectOfType<City>();
        city = this.GetComponent<City>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateDayCount()
    {
        dayText.text = "Day " + city.day.ToString();
    }

    public void UpdateCityData()
    {
        cityText.text = string.Format(
            "Jobs: {0}/{1}\nCash: {2} (+${3})\nPopulation: {4}/{5}\nFood: {6}",
            city.jobsCurrent,
            city.jobsMax,
            city.cash,
            city.cashPerTurn,
            (int)city.populationCurrent,
            (int)city.populationMax,
            (int)city.food);
    }
}
