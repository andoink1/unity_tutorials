﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingController : MonoBehaviour
{
    [SerializeField]
    private City city;
    [SerializeField]
    private UIController uiController;
    [SerializeField]
    private Building[] buildings;
    [SerializeField]
    private Board board;
    private Building selectedBuilding;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0)
            && Input.GetKey(KeyCode.LeftShift)
            && selectedBuilding != null)
            InteractWithBoard(0);
        else if (Input.GetMouseButtonDown(0) && selectedBuilding != null)
            InteractWithBoard(0);
        else if (Input.GetMouseButtonDown(1))
            InteractWithBoard(1);
    }

    // Get building prefab
    public void EnableBuilder(int building)
    {
        selectedBuilding = buildings[building];
    }

    public void InteractWithBoard(int action)
    {
        // Raycast from mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            // Determine grid position for raycast
            Vector3 gridPosition = board.CalculateGridPosition(hit.point);
            
            // If mouse is not over a GameObject (building) and no building at grid position,
            // then place new building (if eligible)
            if (action == 0 && board.CheckForBuildingAtPosition(gridPosition) == null)
            {
                // If mouse is not over a GameObject (building)
                if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                    if (city.cash >= selectedBuilding.cost)
                    {
                        // Deduct cash, update data, increment building count, and add building
                        city.cash -= selectedBuilding.cost;
                        uiController.UpdateCityData();
                        city.buildingCounts[selectedBuilding.id]++;
                        board.AddBuilding(selectedBuilding, gridPosition);
                    }
                }
            }
            else if (action == 1)
            {
                Building buildingToDelete = board.CheckForBuildingAtPosition(gridPosition);
                if (buildingToDelete != null)
                {
                    city.cash += buildingToDelete.cost / 2;
                    board.RemoveBuilding(gridPosition);
                    uiController.UpdateCityData();
                }
            }
        }
    }
}
