﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class City : MonoBehaviour
{
    /// <summary>
    /// Formulas:
    /// - JobsMax = FactoryCount * 10
    /// - JobsCurrent = Min(PopulationCurrent, JobsMax)
    /// - PopulationMax = HouseCount * 5
    /// - PopulationCurrent = Min(PopulationCurrent *= FoodCurrent * 0.2f, PopulationMax)
    /// - FoodCurrent = FoodCurrent + FarmCount * 4
    /// - Cash = Cash + JobsCurrent * 2
    /// </summary>

    public int cash;
    public int cashPerTurn;
    public int day;
    public float populationMax;
    public float populationCurrent;
    public int jobsMax;
    public int jobsCurrent;
    public float food;
    public int[] buildingCounts = new int[4];       // Road, House, Farm, Factory

    private UIController uiController;

    // Start is called before the first frame update
    void Start()
    {
        uiController = GetComponent<UIController>();

        cash = 5000;

        // Temp starting numbers
        //buildingCounts[0] = 0;      //roads
        //buildingCounts[1] = 4;      //houses
        //buildingCounts[2] = 2;      //farms
        //buildingCounts[3] = 3;      //factories
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EndTurn()
    {
        Debug.Log("End Day " + day);
        day++;

        CalculatePopulation();
        CalculateJobs();
        CalculateFood();
        CalculateCash();

        uiController.UpdateCityData();
        uiController.UpdateDayCount();

        Debug.LogFormat("Jobs: {0}/{1}, Cash: {2} (+${3}), Population: {4}/{5}, Food: {6}",
            jobsCurrent, jobsMax, cash, cashPerTurn, populationCurrent, populationMax, food);
    }

    private void CalculatePopulation()
    {
        populationMax = buildingCounts[1] * 5;

        // If more food than population, consume food and grow population
        if (food >= populationCurrent && populationCurrent < populationMax)
        {
            food -= populationCurrent * 0.25f;
            populationCurrent = Mathf.Min(populationCurrent += food * 0.25f, populationMax);
        }
        // If less food than population, decrease population
        else if (food < populationCurrent)
        {
            populationCurrent -= food * 0.5f;
        }
    }

    private void CalculateFood()
    {
        // Increase food by 4 for each Farm
        food += buildingCounts[2] * 4f;
    }

    private void CalculateJobs()
    {
        // Increase max jobs by 4 for each Factory
        jobsMax = buildingCounts[3] * 10;
        jobsCurrent = Mathf.Min((int)populationCurrent, jobsMax);
    }

    private void CalculateCash()
    {
        // Increase cash by 2 for each current job
        cashPerTurn += jobsCurrent * 2;
        cash += cashPerTurn;
    }

    
}
